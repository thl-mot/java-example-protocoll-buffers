package protokollbuffer;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

import org.junit.Test;

import com.lauerbach.examples.protocolbuffers.DataProtos.Data1;
import com.lauerbach.examples.protocolbuffers.DataProtos.Data2;

public class TestProtocolBuffersData {

	
	@Test
	public void test1() throws Exception {
		Data1.Builder ft= Data1.newBuilder();
		
		ft.setEmail("");
		ft.setId(0x1);
		ft.build();
		
		ByteArrayOutputStream arrayStream= new ByteArrayOutputStream();
		
		FileOutputStream output = new FileOutputStream("data1.bin");
	    ft.build().writeTo(output);
	    output.close();
	    
	    ft.build().writeTo( arrayStream);
	    System.out.println( arrayStream.size());
	    arrayStream.close();
	}

	@Test
	public void test2() throws Exception {
		Data2.Builder ft= Data2.newBuilder();
		
		ft.setEmail("");
		ft.setId(0x1);
		ft.build();
		
		ByteArrayOutputStream arrayStream= new ByteArrayOutputStream();
		
		FileOutputStream output = new FileOutputStream("data2.bin");
	    ft.build().writeTo(output);
	    output.close();
	    
	    ft.build().writeTo( arrayStream);
	    System.out.println( arrayStream.size());
	    arrayStream.close();
	}

	
}
